package br.com.agenda.agenda.gui;

import android.app.AlertDialog;
import android.content.Context;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import br.com.agenda.agenda.database.AgendaDatabase;
import br.com.agenda.agenda.database.dao.CompromissoDAO;
import br.com.agenda.agenda.model.Compromisso;
import br.com.agenda.agenda.gui.adapter.ListaCompromissoAdapter;
import br.com.agenda.agenda.model.Usuario;

public class ListaCompromissoView {

    private final ListaCompromissoAdapter adapter;
    private final CompromissoDAO dao;
    private final Context context;

    public ListaCompromissoView(Context context) {
        this.context = context;
        this.adapter = new ListaCompromissoAdapter(this.context);
        dao = AgendaDatabase.getInstance(context).getRoomCompromissoDAO();
    }

    public void confirmaRemocao(final MenuItem item) {
        new AlertDialog
                .Builder(context)
                .setTitle("Removendo compromisso")
                .setMessage("Tem certeza que quer remover esse compromisso da agenda?")
                .setPositiveButton("Sim", (dialogInterface, i) -> {
                    AdapterView.AdapterContextMenuInfo menuInfo =
                            (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                    Compromisso compromissoEscolhido = adapter.getItem(menuInfo.position);
                    remove(compromissoEscolhido);
                })
                .setNegativeButton("Não", null)
                .show();
    }

    public void atualizaCompromissos() {
        adapter.atualiza(dao.listarPorIdUsuario(Usuario.usuarioSession.getId()));
    }

    private void remove(Compromisso compromisso) {
        dao.remove(compromisso);
        adapter.remove(compromisso);
    }

    public void configuraAdapter(ListView listaDeCompromisso) {
        listaDeCompromisso.setAdapter(adapter);
    }
}
