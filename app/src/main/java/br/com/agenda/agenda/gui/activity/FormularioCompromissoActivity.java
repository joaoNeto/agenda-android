package br.com.agenda.agenda.gui.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import br.com.agenda.agenda.R;
import br.com.agenda.agenda.database.AgendaDatabase;
import br.com.agenda.agenda.database.dao.CompromissoDAO;
import br.com.agenda.agenda.model.Compromisso;
import br.com.agenda.agenda.model.Usuario;

import static br.com.agenda.agenda.gui.activity.ConstantesActivities.CHAVE_ALUNO;

public class FormularioCompromissoActivity extends AppCompatActivity {

    private static final String TITULO_APPBAR_NOVO_ALUNO = "Novo compromisso";
    private static final String TITULO_APPBAR_EDITA_ALUNO = "Editar compromisso";
    private static final int TIRAR_FOTO = 1;
    private EditText tituloCompromisso;
    private EditText dataCompromisso;
    private EditText descricaoCompromisso;
    private CompromissoDAO dao;
    private Compromisso compromisso;
    private DatePickerDialog.OnDateSetListener setListener;
    private Button btnGetData;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_compromisso);
        AgendaDatabase database = AgendaDatabase.getInstance(this);
        dao = database.getRoomCompromissoDAO();
        inicializacaoDosCampos();
        carregaDadosCompromisso();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater()
                .inflate(R.menu.activity_formulario_aluno_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == R.id.activity_formulario_aluno_menu_salvar){
            finalizaFormulario();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.gc();
         if (requestCode == TIRAR_FOTO && resultCode == RESULT_OK) {
            try {

                Bundle extras = data.getExtras();
                Bitmap imagem = (Bitmap) extras.get("data");
                imageView.setImageBitmap(imagem);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                imagem.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte imagemBytes[] = stream.toByteArray();
                compromisso.setImagem(imagemBytes);
            } catch (Exception e) {
                new AlertDialog
                        .Builder(FormularioCompromissoActivity.this)
                        .setTitle("Erro")
                        .setMessage("Erro ao fazer upload do arquivo.")
                        .show();
            }
         }
    }

    private void carregaDadosCompromisso() {
        Intent dados = getIntent();
        Calendar calendar = Calendar.getInstance();
        int yearData  = calendar.get(Calendar.YEAR);
        int monthData = calendar.get(Calendar.MONTH);
        int dayData   = calendar.get(Calendar.DAY_OF_MONTH);

        if (dados.hasExtra(CHAVE_ALUNO)) {
            setTitle(TITULO_APPBAR_EDITA_ALUNO);
            compromisso = (Compromisso) dados.getSerializableExtra(CHAVE_ALUNO);
            tituloCompromisso.setText(compromisso.getTitulo());
            descricaoCompromisso.setText(compromisso.getDescricao());
            dataCompromisso.setText(compromisso.getStringData());
            Calendar c = compromisso.getData();
            yearData  = c.get(Calendar.YEAR);
            monthData = c.get(Calendar.MONTH);
            dayData   = c.get(Calendar.DAY_OF_MONTH);
            try{
                // carregar imagem
                byte[] imagemByte = compromisso.getImagem();
                ByteArrayInputStream stream = new ByteArrayInputStream(imagemByte);
                Bitmap img = BitmapFactory.decodeStream(stream);
                imageView.setImageBitmap(img);
            }catch(Exception ex){}

        } else {
            setTitle(TITULO_APPBAR_NOVO_ALUNO);
            compromisso = new Compromisso();
        }

        final int year  = yearData;
        final int month = monthData;
        final int day   = dayData;
        btnGetData.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                    FormularioCompromissoActivity.this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    setListener,
                    year,
                    month,
                    day
                );
                datePickerDialog.getWindow().setBackgroundDrawable( new ColorDrawable(Color.TRANSPARENT) );
                datePickerDialog.show();
            }
        });

        setListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dataFinal = dayOfMonth+"/"+(month+1)+"/"+year;
                dataCompromisso.setText(dataFinal);
                compromisso.setData(dataFinal);
            }
        };

        imageView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                try{
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, TIRAR_FOTO);
                }catch(Exception ex){
                    Log.i("ERROU", ex.getMessage());
                    new AlertDialog
                            .Builder(FormularioCompromissoActivity.this)
                            .setTitle("Erro")
                            .setMessage("Erro ao tentar capturar a camera")
                            .show();
                }
            }
        });

    }

    private void finalizaFormulario() {
        setValoresObjCompromisso();
        if (compromisso.temIdValido()) {
            dao.edita(compromisso);
        } else {
            dao.salva(compromisso);
        }
        finish();
    }

    private void inicializacaoDosCampos() {
        tituloCompromisso    = findViewById(R.id.tituloCompromisso);
        dataCompromisso      = findViewById(R.id.dataCompromisso);
        descricaoCompromisso = findViewById(R.id.descricaoCompromisso);
        btnGetData           = findViewById(R.id.btnGetData);
        imageView            = findViewById(R.id.imageView);
    }

    private void setValoresObjCompromisso() {
        String titulo = tituloCompromisso.getText().toString();
        String data = dataCompromisso.getText().toString();
        String descricao = descricaoCompromisso.getText().toString();

        compromisso.setTitulo(titulo);
        compromisso.setData(data);
        compromisso.setDescricao(descricao);
        compromisso.setIdUsuario(Usuario.usuarioSession.getId());
    }
}
