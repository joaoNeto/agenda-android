package br.com.agenda.agenda.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Entity
public class Compromisso implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id = 0;
    private int idUsuario;
    private String titulo;
    private Calendar data;
    private String descricao;
    private byte[] imagem;
    private Calendar momentoDeCadastro = Calendar.getInstance();

    // GETTERS E SETTERS DO ID
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public boolean temIdValido() {
        return id > 0;
    }

    // GETTERS E SETTERS DO ID DO USUÁRIO
    public int getIdUsuario() { return idUsuario; }
    public void setIdUsuario(int idUsuario) { this.idUsuario = idUsuario; }

    // GETTERS E SETTERS DO TITULO
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getTitulo() {
        return titulo;
    }

    // GETTERS E SETTERS DA DATA
    public void setData(Calendar data) { this.data = data; }
    public void setData(String strDate) {
        /* pega a data no formato DD/MM/YYYY */
        Calendar c;
        try{
            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            c = Calendar.getInstance();
            c.setTime(sdf.parse(strDate));
        } catch (ParseException e) {
            String[] arrData = strDate.split("/");
            c = (Calendar) Calendar.getInstance();
            c.set(Calendar.MONTH, Integer.parseInt(arrData[0])  );
            c.set(Calendar.YEAR, Integer.parseInt(arrData[1]) );
            c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(arrData[2]) );
        }
        this.data = c;
    }
    public Calendar getData() {
        return data;
    }
    public String getStringData() { return new SimpleDateFormat("dd/MM/yyyy").format(this.data.getTime()); }

    // GETTERS E SETTERS DA DESCRIÇÃO
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public String getDescricao() {
        return descricao;
    }

    // GETTERS E SETTERS DO MOMENTO DE CADASTRO
    public void setMomentoDeCadastro(Calendar momentoDeCadastro) { this.momentoDeCadastro = momentoDeCadastro; }
    public Calendar getMomentoDeCadastro() {
        return momentoDeCadastro;
    }

    public byte[] getImagem() {
        return imagem;
    }

    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }

    @NonNull
    @Override
    public String toString() {
        return titulo + " - " + data;
    }
}
