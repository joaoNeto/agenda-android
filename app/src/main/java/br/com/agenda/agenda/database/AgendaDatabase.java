package br.com.agenda.agenda.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import br.com.agenda.agenda.database.converter.ConversorCalendar;
import br.com.agenda.agenda.database.dao.CompromissoDAO;
import br.com.agenda.agenda.database.dao.UsuarioDAO;
import br.com.agenda.agenda.model.Compromisso;
import br.com.agenda.agenda.model.Usuario;

@Database(entities = {Compromisso.class, Usuario.class}, version = 1, exportSchema = false)
@TypeConverters({ConversorCalendar.class})
public abstract class AgendaDatabase extends RoomDatabase {

    private static final String NOME_BANCO_DE_DADOS = "agenda.db";
    public abstract CompromissoDAO getRoomCompromissoDAO();
    public abstract UsuarioDAO getRoomUsuarioDAO();

    public static AgendaDatabase getInstance(Context context) {
        return Room
                .databaseBuilder(context, AgendaDatabase.class, NOME_BANCO_DE_DADOS)
                .allowMainThreadQueries()
                .build();
    }
}
