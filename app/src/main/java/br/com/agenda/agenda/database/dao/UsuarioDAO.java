package br.com.agenda.agenda.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.agenda.agenda.model.Usuario;

@Dao
public interface UsuarioDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long salva(Usuario usuario);

    @Query("SELECT * FROM usuario")
    List<Usuario> todos();

    @Query("SELECT * FROM usuario WHERE login LIKE :login and senha LIKE :senha ")
    List<Usuario> buscar(String login, String senha);

    @Delete
    void remove(Usuario usuario);

    @Update
    void edita(Usuario usuario);
}
