package br.com.agenda.agenda.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Calendar;

@Entity
public class Usuario implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id = 0;
    private String login;
    private String email;
    private String senha;
    private Calendar momentoDeCadastro = Calendar.getInstance();

    @Ignore
    public static Usuario usuarioSession = new Usuario();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Calendar getMomentoDeCadastro() {
        return momentoDeCadastro;
    }

    public void setMomentoDeCadastro(Calendar momentoDeCadastro) {
        this.momentoDeCadastro = momentoDeCadastro;
    }

    @NonNull
    @Override
    public String toString() {
        return "";
    }
}
