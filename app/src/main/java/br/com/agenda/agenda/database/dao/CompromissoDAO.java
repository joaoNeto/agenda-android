package br.com.agenda.agenda.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.agenda.agenda.model.Compromisso;

@Dao
public interface CompromissoDAO {
    @Insert
    void salva(Compromisso compromisso);

    @Query("SELECT * FROM Compromisso")
    List<Compromisso> todos();

    @Query("SELECT * FROM Compromisso where idUsuario = :idUsuario") // order by data asc
    List<Compromisso> listarPorIdUsuario(int idUsuario);

    @Delete
    void remove(Compromisso compromisso);

    @Update
    void edita(Compromisso compromisso);
}
