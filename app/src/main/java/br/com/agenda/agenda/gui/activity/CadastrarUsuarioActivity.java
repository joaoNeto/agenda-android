package br.com.agenda.agenda.gui.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import br.com.agenda.agenda.R;
import br.com.agenda.agenda.database.AgendaDatabase;
import br.com.agenda.agenda.database.dao.UsuarioDAO;
import br.com.agenda.agenda.model.Usuario;

public class CadastrarUsuarioActivity extends AppCompatActivity {

    private UsuarioDAO usuarioDao;
    private EditText loginForm;
    private EditText emailForm;
    private EditText senhaForm;
    private Button cadastrarBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_usuario);
        setTitle("Cadastrar usuário");
        AgendaDatabase database = AgendaDatabase.getInstance(this);
        usuarioDao = database.getRoomUsuarioDAO();

        loginForm = (EditText) findViewById(R.id.loginForm);
        emailForm = (EditText) findViewById(R.id.emailForm);
        senhaForm = (EditText) findViewById(R.id.senhaForm);
        cadastrarBtn = (Button) findViewById(R.id.cadastrarBtn);

        cadastrarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cadastrarUsuario();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater()
                .inflate(R.menu.activity_cadastro_usuario_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == R.id.activity_cadastrar_usuario_voltar){
            startActivity(new Intent( CadastrarUsuarioActivity.this, LoginActivity.class ));
        }
        return super.onOptionsItemSelected(item);
    }

    private void cadastrarUsuario(){
        Usuario usuario = new Usuario();
        usuario.setLogin(loginForm.getText().toString());
        usuario.setEmail(emailForm.getText().toString());
        usuario.setSenha(senhaForm.getText().toString());
        if(usuarioDao.salva(usuario) > 0){
            List<Usuario> listaUsuario = usuarioDao.buscar(loginForm.getText().toString(), senhaForm.getText().toString());
            Usuario.usuarioSession = listaUsuario.get(0);
            startActivity(new Intent( CadastrarUsuarioActivity.this, ListaCompromissoActivity.class ));
        } else {
            new AlertDialog
                    .Builder(CadastrarUsuarioActivity.this)
                    .setTitle("Erro ao cadastrar usuário!")
                    .setMessage("Erro ao cadastrar usuário!")
                    .show();
        }
    }

}
