package br.com.agenda.agenda.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import br.com.agenda.agenda.R;
import br.com.agenda.agenda.model.Compromisso;
import br.com.agenda.agenda.gui.ListaCompromissoView;

import static br.com.agenda.agenda.gui.activity.ConstantesActivities.CHAVE_ALUNO;

public class ListaCompromissoActivity extends AppCompatActivity {

    private static final String TITULO_APPBAR = "Minha agenda";
    private ListaCompromissoView listaCompromissoView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_compromisso);
        setTitle(TITULO_APPBAR);
        listaCompromissoView = new ListaCompromissoView(this);
        configuraFabNovoAluno();
        configuraLista();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater()
                .inflate(R.menu.activity_lista_alunos_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == R.id.activity_lista_alunos_menu_remover) {
            listaCompromissoView.confirmaRemocao(item);
        }

        return super.onContextItemSelected(item);
    }

    private void configuraFabNovoAluno() {
        FloatingActionButton botaoNovoAluno = findViewById(R.id.activity_lista_alunos_fab_novo_aluno);
        botaoNovoAluno.setOnClickListener(view -> abreFormularioModoInsereAluno());
    }

    private void abreFormularioModoInsereAluno() {
        startActivity(new Intent(this, FormularioCompromissoActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        listaCompromissoView.atualizaCompromissos();
    }

    private void configuraLista() {
        ListView listaDeAlunos = findViewById(R.id.activity_lista_alunos_listview);
        listaCompromissoView.configuraAdapter(listaDeAlunos);
        configuraListenerDeCliquePorItem(listaDeAlunos);
        registerForContextMenu(listaDeAlunos);
    }

    private void configuraListenerDeCliquePorItem(ListView listaDeAlunos) {
        listaDeAlunos.setOnItemClickListener((adapterView, view, posicao, id) -> {
            Compromisso compromissoEscolhido = (Compromisso) adapterView.getItemAtPosition(posicao);
            abreFormularioModoEditaAluno(compromissoEscolhido);
        });
    }

    private void abreFormularioModoEditaAluno(Compromisso compromisso) {
        Intent vaiParaFormularioActivity = new Intent(ListaCompromissoActivity.this, FormularioCompromissoActivity.class);
        vaiParaFormularioActivity.putExtra(CHAVE_ALUNO, compromisso);
        startActivity(vaiParaFormularioActivity);
    }

}
