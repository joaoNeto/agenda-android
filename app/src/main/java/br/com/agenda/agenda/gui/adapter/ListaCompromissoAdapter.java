package br.com.agenda.agenda.gui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.agenda.agenda.R;
import br.com.agenda.agenda.model.Compromisso;

public class ListaCompromissoAdapter extends BaseAdapter {

    private final List<Compromisso> compromissos = new ArrayList<>();
    private final Context context;

    public ListaCompromissoAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return compromissos.size();
    }

    @Override
    public Compromisso getItem(int posicao) {
        return compromissos.get(posicao);
    }

    @Override
    public long getItemId(int posicao) {
        return compromissos.get(posicao).getId();
    }

    @Override
    public View getView(int posicao, View view, ViewGroup viewGroup) {
        View viewCriada = criaView(viewGroup);
        Compromisso compromissoDevolvido = compromissos.get(posicao);
        vincula(viewCriada, compromissoDevolvido);
        return viewCriada;
    }

    private void vincula(View view, Compromisso compromisso) {
        TextView nome = view.findViewById(R.id.item_aluno_nome);
        nome.setText(compromisso.getTitulo());
        TextView data = view.findViewById(R.id.item_aluno_telefone);
        data.setText(compromisso.getStringData());

        // carregar imagem
        try{
            ImageView imageView = view.findViewById(R.id.iconListCompromisso);
            byte[] imagemByte = compromisso.getImagem();
            ByteArrayInputStream stream = new ByteArrayInputStream(imagemByte);
            Bitmap img = BitmapFactory.decodeStream(stream);
            imageView.setImageBitmap(img);
        }catch(Exception ex){}
    }

    private View criaView(ViewGroup viewGroup) {
        return LayoutInflater
                .from(context)
                .inflate(R.layout.listview_item_compromisso, viewGroup, false);
    }

    public void atualiza(List<Compromisso> compromissos){
        this.compromissos.clear();
        this.compromissos.addAll(compromissos);
        notifyDataSetChanged();
    }

    public void remove(Compromisso compromisso) {
        compromissos.remove(compromisso);
        notifyDataSetChanged();
    }
}
